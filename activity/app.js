// https://jsonplaceholder.typicode.com/posts
// fetcch - used to perfrom crud operations in a given url
    // accepts 2 arguments - url(needed) and options (not a rquirement)
        //  options parameter - used only when the dev needs the request body from the user

/* 
    use fetch method to get the posts inside the https://jsonplaceholder.typicode.com/posts 
    make the response in json format 
*/
fetch("https://jsonplaceholder.typicode.com/posts")
    .then(response => response.json())
    .then(data => showPost(data))

//  add post

document.querySelector("#form-add-post").addEventListener('submit', (e) => {
    e.preventDefault()

    fetch("https://jsonplaceholder.typicode.com/posts", {
        method: "POST",
        body: JSON.stringify({
            title: document.querySelector("#txt-title").value,
            body: document.querySelector("#txt-body").value,
            userId:1
        }),
        headers:{"Content-Type" : "application/json; charset=UTF-8"}
    })

    .then(response => response.json()) // converts the response into JSON format
.then(data => {
    console.log(data)
    alert("Post created successfully")

    // to clear the text in the input fields upon creating a post
    document.querySelector('#txt-title').value = null
    document.querySelector('#txt-body').value = null
    }) 

})

/* 

    create a showPost function that will display the posts in the place holder database as well as the posts created iun the webpage (priority - to display the created posts)
        the posts should appear in the div under the "posts"

        send a screenshot of the output in the batch hangouts/ google chat
*/

const showPost = (posts) => {
    let postEntries = "";

    posts.forEach((post) => {
        // get each element one by one
        postEntries += `
            <div id = "post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>

                <p id="post-body-${post.id}">${post.body}</p>
                <button onclick="editPost('${post.id}')">edit</button>
                <button onclick="deletePost('${post.id}')">delete</button>
            </div>
        `
    })
    document.querySelector("#post-entries").innerHTML = postEntries
}


// edit post function

const editPost = (id) => { 
    let title = document.querySelector(`#post-title-${id}`).innerHTML
    let body = document.querySelector(`#post-body-${id}`).innerHTML

    document.querySelector('#txt-edit-id').value = id 
    document.querySelector('#txt-edit-title').value = title 
    document.querySelector('#txt-edit-body').value = body

    // to remove the atribute from the element; it receivess a string argument that serves be the attrribute of the element
    document.querySelector('#btn-submit-update').removeAttribute("disabled")
}


// update post

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
    e.preventDefault()  
    fetch("https://jsonplaceholder.typicode.com/posts/1", {
        method: "PUT",
        body: JSON.stringify({
            id: document.querySelector('#txt-edit-id').value,
            title: document.querySelector('#txt-edit-title').value,
            body: document.querySelector('#txt-edit-body').value,
            userId: 1
        }),
        headers:{"Content-Type" : "application/json; charset=UTF-8"}
    }) 
    .then(response => response.json())
    .then(data => {
        console.log(data)
        alert("post successfully updated")
        // clears the input fields
        document.querySelector('#txt-edit-title').value = null
        document.querySelector('#txt-edit-body').value = null
        document.querySelector('#txt-edit-body').value = null
        // 
        document.querySelector("#btn-submit-update").setAttribute('disabled', true)
    })
}) 

/* 
    Activity : Make the "Delete button work"
    only the post which delete button is presesd sh
*/

const deletePost = (id) => {
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {method: 'DELETE'})
    document.querySelector(`#post-${id}`).remove()
}